# Sistemes Operatius en xarxes
## Practica Windows server Active Directory
**Index**<br>
    - [Instalació de active Directory en el Windows server](#id1)<br>
    - [Creació de domini](#id2)<br>
    - [Questions](#id3)

### Instalació de Active Directory Server <a name="id1"></a>
* Començem per montar la infraestructura de domini per la suposada empresa CFEDT07, per aixo instalarem els serveix de Active Directory per asignar-li el rol de controlador de domini del Active directory i continuar els pasos del Asistent de rols fins poder instalar-lo.(fig 1.1)

![Imagen del Active Directory](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_0.png)

Fig 1.1.Escollin el rol de domini del Active directory.

* Quan instalem el Active directory ens mostrara un warning que ens demana crear un domini per el nostre Active Directory.(fig 1.2)

![Imagen del warning del Active Directory](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_1.png)

Fig 1.2.Mostra els requisist per funcionar el Active Directory.

### Creació de domini <a name="id2"></a>
* Quan comenzem a crear el domini ens dona 3 opcions, el primer per agregar un domini ja creat i els dos ultims es crear domini pero la diferencia es que un es per crear un bosc i el altre es per agregar-lo en el bosc.(fig 2.1)(fig 2.2)

![Creació de domini](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_2.png)

Fig 2.1.Creació del un nou bosc

![Creació de domini pt2](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practiva_WS_AD_5.png)

Fig 2.2. Posan el nom de la empresa CFEDT07
* Ens demana escollir el nivell de funcionalitat, que seria del 2016(fig 2.3) i una contrasenya per poder accedir al mode restauració per si es borra o es danya el domini. (fig 2.4) Després ens mostrara un warning que ens diu que no pot crear una delegació del DNS.

![Contrasenya del domini](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_3.png)


Fig 2.3.Escollim el nivell de funcinalitat i la contrasenya

![Contrasenya del domini](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practiva_WS_AD_4.png)

Fig 2.4.Creació de delegació(DNS)

* Especifiquem la ruta de acces del directori de la base de dades, els arxius de registre i SYSVOL (Es un conjunt de arxius carpetes que residen en el disc)(fig 2.5)

![Ruta de acces](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_6.png)

Fig 2.5.Eleccions de rutes de access pel active directory
* Quan estem per finalitzar ens mostra les opcions que s'instalaran en el nostre server (també podem veure en raw desde un editor de text).(fig 2.6)

![server](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_7.png)

Fig 2.6.Revisió de les seleccions escollides.
* Després ens mostra diferents warning que per exemple ens diu que no hi ha un DNS delegat en aquest domini pero que se creara de totes formes.(fig 2.7)

![warning fatal dns per el domini](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_WS_AD/Practica_WS_AD_8.png)

Fig 2.7. Requisist per que funciones correctament el Active Directory
### Questions <a name="id3"></a>
* En cas de tenir algun altre servidor amb windows 2003 instal·lat, quin nivell funcional hauríem d'utilitzar?
[Wiki on lo he trobat](https://forsenergy.com/es-es/domadmin/html/6e36265c-863a-4f03-92b9-ee994e61b34f.htm#:~:text=En%20AD%20DS%20de%20Windows%20Server%202008%20R2%2C,dominio%20y%20sus%20controladores%20de%20dominio%20compatibles%20correspondientes%3A)

    - Diu que el nivell de funcionalitat de la versió del 2003 es millor deixar el nivell de windows server 2003 predeterminat en el windows server.

* A la següent pantalla indiquem la ubicació dels arxius de la base de dades. podem modificar el destí? i el seu nom?. És també obligatori que la partició sigui de tipus NTFS.

    - Podem modificar el destí on es crear la base de dades, el nom se posible pero tenim que fer a part de la creació de la ruta. Es obligatori i necesari fer la partició en NTFS si utilisem Windows tant si es el server o el de escritori.