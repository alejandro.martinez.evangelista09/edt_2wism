# Pràctica - Usuaris i Grups

## Index<br>
  * [Creació](#ida1)
  * [Documentació](#ida2)
    * [Creació de plantilla](#ida2-1)
    * [Especificació de seguretat](#ida2-2)
    * [Que tenir a compte?](#ida2-3)

## Creació:<a name=ida1></a>

1.  Crear dos unitats organitzatives noves, “Institut” i “UOdeRelleno”, dins del nostre
        domini.
    
    2.  Elimina l'UOdeRelleno
    
    3.  A l'UO restant crea els següents usuaris:
        1.  Tot el professorat que teniu.
        2.  Almenys 5 companys/es de l'aula
        3.  7 "alumnes de relleno", podeu inventar-se els nomss
4.  Crea els següents grups. Poden ser de l'ambit que considereu, reviseu els criteris posteriors per incompatibilitats:
    1.  EscolaDelTreball
    2.  Professorat
    3.  Inf
    4.  Alumnat
    5.  GrupDeRelleno
5.  Inclou a tots els grups dins de EscolaDelTreball
    
6.  Inclou als usuaris als seus corresponents grups. Els profes a profes, profes d’inf a inf (Anglés no pertany a inf). 
        Els alumnes de relleno també son alumnes.
    
7.  Elimina el GrupDeRelleno i un AlumneDeRelleno (Fixat que passa amb la resta).
    
    8.  Creem el recurs compartir C:\Apunts amb els permisos corresponents:
        1.  Els alumnes poden llegir els apunts.
        2.  Els professors poden editar i llegir els apunts.
    
    9.  Creem el recurs compartir 
    
    C:\Public amb els permisos corresponents:
        1.  Els alumnes poden crear i modificar però no esborrar.
        2.  El tutor i el delegat del grup poden esborrar continguts.

## Documentació:<a name=ida2></a>

### Creació de plantilles d'usuaris<a name=ida2-1></a>
* Dons quan ya has creat la unitat organitzativa de nom "institut" tenim que crear els usuaris i això ens el facilitara una plantilla per posar algunes configurancions ya predefinides.

![Usuarios y equipos de Active Directory](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_usuaris_1.png)
![Creacio de unitat formativa](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_usuaris_2.png)
* Per crear una plantilla es facil, es molt paregut a la creació dels usuaris(Fig1.1)(Fig1.2) pero la diferencia seria que tenim que borrar el marcador "Cuenta desabilitada" perqué es pugui veure en les llistes.

![Image de plantilla](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/plantilla.png)
![Image de plantilla 2](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/plantilla_2.png)

### Especificació de seguretat<a name=ida2-2></a>
* En windows server tenim que cumplir uns requisits per poder posar contrasenyes que com a minim ha de tenir 3 tipus de caracteres (minuscules, majuscules y numeros)com per ejemple "Potato124"

![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grup_usuari3.png)
![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_gru_usuaris4.png)

* Si volem canviar les politiques seria posan gpedit.msc en la finestre de exe, i ens portara en el repositori on esta el directori "Directiva de contraseñas" 


![gpedit](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/gpedit.png)
### Que tenir en compte.<a name=ida2-3></a>

* Per crear usuaris en el windows server en interficie grafica es molt facil, sols tenim que anar a "Usuarios y equipos del Active Directory" i entrar en la unitat organitzativas on creem els usuaris o grups.

![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_i_usuaris_5.png)
* Ens demanara uns requisits per crear usuaris que seria nom, cognom, un nom d'inici i un password que porti 3 tipus de caracteres (per defecte).

![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_usuaris_6.png)
![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_usuaris_7.png)
![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_usuaris_8.png)
* I crear grups es més facil sols tenim que posar el nom asegurar en el ambit(Entre Domini local, global o universal) i el tipus de grup (Seguridad o Distribucción), compte quan la cream perqué podem crear-la sense poder moure cap cosa en aquell grup.
![](https://gitlab.com/alejandro.martinez.evangelista09/edt_2wism/-/raw/main/M04/Practica_grup_usuaris/Practica_grups_usuaris_9.png)