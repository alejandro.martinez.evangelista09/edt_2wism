# M06 Seguridad informatica

##  Pildora 2 Backup en red
* Cuando hacemos un backup a la datos que tienes lo idea seria que enviarlo a una
maquina donde no este la original. Lo mejor seria es tranferir los datos a traves de 
la red a la maquina. Para eso existe los nas que serian dispositivos que almacena 
datos de otros dispositivos que estan conectados por red.
## Rsync: Sincronización de datos en la red

* El rsync es un programa para sincronizar ficheros para cuando un fichero ha sido 
modificado que el otro fichero es que sincronizado pueda copiar todo tanto lo 
modificado como lo que no esta.

* Ser puede hacer tanto en local como en servidores.

### Rsync Local
El Rsync en local sirve igual que el "copia y pega". En este modo se entender que es más facil copiar todo que calcular las diferencias entre la copia del origen que la 
del destino.
### Rsync Remoto
Para el Rsync remoto es necesario instalar-lo tanto el origen como en el equipo remoto

* Hay dos forma de utilizarlo.
    * Como demonio(demon): Haria las transferencias utilizando el puerto TCP/873. 
    También puede ejecutarse de forma independiente o mediante la utilizacíón de inetd.
    * Con una shell remota: Utilizariamos una shell remota como ssh o rsh para poder ejecutarse una vez que se haya iniciado sesión en el equipo remoto.

## ¿Cómo funciona Rsync?
Sintaxia

rsync -opciones origen destino

En local:

cd /home/carmen/rsync
rsync archivo1.txt archivo2.txt

* También podemos transferir los archivos entre diferentes hosts, siempre que entre 
ellos tengan visibilidad usando red y se usara el SSH por defecto para mover datos.
Es prescindible instalar el Rsync en el host destino aun teniendo el servidor ssh funcionando.

rsync -e "ssh -p 1234" archivo1 usuario@host.dominio:/tmp/

## Ventajas de Rsync
* Rsync nos proporciona una interesante lista de opciones que le hacen especialmente
adecuado para transferencias repetitivas (o puntuales)
    
    * Soporta el copiado de permisos de usuarios, grupos, así como enlaces y 
    dispositivos.
    
    * Dispone de opciones de exclusión similares a las de tar.
    
    * No necesita privilegios de root (salvo que sean necesarias para acceder a los 
    orígenes o destinos).
    
    * Soporta la utilización de claves públicas para transferencias automatizadas.
    
    * Si utilizamos el transporte mediante SSH, nuestra transferencia es cifrada de 
    forma automática, dándole mayor seguridad.

